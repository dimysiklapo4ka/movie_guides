package com.example.dmitry_ko.moviesguide.ui.main.listener;

import android.app.Activity;

public interface OnBackPresedListener {
    void onBackPresed(Activity activity);
}
