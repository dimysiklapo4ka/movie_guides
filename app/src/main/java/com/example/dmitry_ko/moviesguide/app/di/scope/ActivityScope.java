package com.example.dmitry_ko.moviesguide.app.di.scope;

import javax.inject.Scope;

@Scope
public @interface ActivityScope {
}
