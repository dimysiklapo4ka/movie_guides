package com.example.dmitry_ko.moviesguide.ui.main.fragment.movies.view_holders;

import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.graphics.Palette;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.bumptech.glide.request.transition.Transition;
import com.example.dmitry_ko.moviesguide.R;
import com.example.dmitry_ko.moviesguide.api.ApiConstants;
import com.example.dmitry_ko.moviesguide.api.model.Movie;
import com.example.dmitry_ko.moviesguide.ui.main.fragment.listeners.MovieItemClickListener;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GridMoviesViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.imageView)ImageView mPoster;
    @BindView(R.id.textView)TextView mName;
    private MovieItemClickListener mListener;
    private Movie movie;

    public GridMoviesViewHolder(@NonNull View itemView) {
        super(itemView);
        ButterKnife.bind(this,itemView);
        itemView.setOnClickListener(new Clicked());
    }

    public void bind(Movie movie, MovieItemClickListener listener){
        mListener = listener;
        this.movie = movie;
        bindView(movie);
    }

    private void bindView(Movie movie){
        mName.setText(movie.getTitle());
        Glide.with(itemView.getContext())
                .asBitmap()
                .load(ApiConstants.getPosterPath(movie.getPosterPath()))
                .into(new BitmapImageViewTarget(mPoster) {
                    @Override
                    public void onResourceReady(Bitmap bitmap, @Nullable Transition<? super Bitmap> transition) {
                        super.onResourceReady(bitmap, transition);
                        Palette.from(bitmap).generate(palette -> setBackgroundColor(palette));
                    }
                });
    }

    private void setBackgroundColor(Palette palette) {
        mName.setBackgroundColor(palette.getVibrantColor(itemView.getContext()
                .getResources().getColor(R.color.black_translucent_60)));
        mName.setAlpha(0.8f);
    }

    private class Clicked implements View.OnClickListener{

        @Override
        public void onClick(View v) {
            mListener.movieClicked(movie);
        }
    }
}
