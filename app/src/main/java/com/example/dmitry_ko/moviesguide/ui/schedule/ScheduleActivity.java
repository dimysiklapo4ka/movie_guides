package com.example.dmitry_ko.moviesguide.ui.schedule;

import com.example.dmitry_ko.moviesguide.R;
import com.example.dmitry_ko.moviesguide.base.BaseActivity;

public class ScheduleActivity extends BaseActivity {
    @Override
    protected void init() {

    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_watching;
    }
}
