package com.example.dmitry_ko.moviesguide.ui.main.fragment.movie_detail.view_holder;

import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.example.dmitry_ko.moviesguide.R;
import com.example.dmitry_ko.moviesguide.api.model.Video;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DetailViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    @BindView(R.id.video)ImageView mTrailer;
    private Video video;

    public DetailViewHolder(@NonNull View itemView) {
        super(itemView);
        ButterKnife.bind(this,itemView);
        itemView.setOnClickListener(this);
    }

    public void bind(Video video){
        this.video = video;
        bindView(video);
    }

    private void bindView(Video video){
        Glide.with(itemView.getContext())
                .load(Video.getThumbnailUrl(video))
                .into(mTrailer);
    }

    @Override
    public void onClick(View v) {
        Intent playVideoIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(Video.getUrl(video)));
        itemView.getContext().startActivity(playVideoIntent);
    }
}
