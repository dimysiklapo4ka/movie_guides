package com.example.dmitry_ko.moviesguide.ui.main.fragment.movies.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.example.dmitry_ko.moviesguide.R;
import com.example.dmitry_ko.moviesguide.api.ApiConstants;
import com.example.dmitry_ko.moviesguide.api.model.Movie;
import com.example.dmitry_ko.moviesguide.ui.main.fragment.listeners.MovieItemClickListener;
import com.example.dmitry_ko.moviesguide.ui.main.fragment.listeners.PaginationLoadMoreListener;
import com.example.dmitry_ko.moviesguide.ui.main.fragment.movies.MoviesFragment;
import com.example.dmitry_ko.moviesguide.ui.main.fragment.movies.view_holders.GridMoviesViewHolder;
import com.example.dmitry_ko.moviesguide.ui.main.fragment.movies.view_holders.LinearMoviesViewHolder;
import com.example.dmitry_ko.moviesguide.ui.main.fragment.movies.view_holders.SingleMoviesViewHolder;

import java.util.ArrayList;
import java.util.List;

public class MoviesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<Movie> mMovies = new ArrayList<>();
    private int TYPE_LAYOUT_MANAGER = 1;
    private MovieItemClickListener mListener;
    private PaginationLoadMoreListener mLoadMoreListener;

    public MoviesAdapter(PaginationLoadMoreListener loadMoreListener){
        mLoadMoreListener = loadMoreListener;
    }

    public void setMovies(List<Movie> movies) {
        if (movies != null && movies.size() > 0 ) {
            mMovies = movies;
            notifyDataSetChanged();
        }
    }

    public void addMovies(List<Movie> movies){
        if (movies != null && movies.size() > 0){
            mMovies.addAll(movies);
            notifyDataSetChanged();
        }
    }

    public String getMovies(int position){
        if (mMovies != null && mMovies.size() != 0) {
            return ApiConstants.getPosterPath(mMovies.get(position).getPosterPath());
        }
        return null;
    }

    public void setTypeLayoutManager(int layoutManager){
        TYPE_LAYOUT_MANAGER = layoutManager;
        notifyDataSetChanged();
    }

    public void setItemClickListener(MovieItemClickListener listener){
        mListener = listener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        if (TYPE_LAYOUT_MANAGER == MoviesFragment.LINEAR_MANAGER) {
            return new LinearMoviesViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.view_linear_movie_item, viewGroup, false));
        }
        if (TYPE_LAYOUT_MANAGER == MoviesFragment.GRID_MANAGER){
            return new GridMoviesViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.view_grid_movie_item, viewGroup, false));
        }
        if (TYPE_LAYOUT_MANAGER == MoviesFragment.SINGLE_MANAGER){
            return new SingleMoviesViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.view_single_movie_item, viewGroup, false));
        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        if (viewHolder instanceof LinearMoviesViewHolder){
            LinearMoviesViewHolder holder = (LinearMoviesViewHolder) viewHolder;
            holder.bind(mMovies.get(i), mListener);
        }else if (viewHolder instanceof GridMoviesViewHolder){
            GridMoviesViewHolder holder = (GridMoviesViewHolder) viewHolder;
            holder.bind(mMovies.get(i), mListener);
        }else if (viewHolder instanceof SingleMoviesViewHolder){
            SingleMoviesViewHolder holder = (SingleMoviesViewHolder) viewHolder;
            holder.bind(mMovies.get(i), mListener);
        }
        if (i == (mMovies.size()-1)){
            Log.e(MoviesAdapter.class.getSimpleName(), "onBindViewHolder: true " + String.format("position -> %d == list.size %d", i, mMovies.size()-1));
            mLoadMoreListener.loadMoreMovies();
        }
    }

    @Override
    public int getItemCount() {
        return mMovies.size();
    }
}
