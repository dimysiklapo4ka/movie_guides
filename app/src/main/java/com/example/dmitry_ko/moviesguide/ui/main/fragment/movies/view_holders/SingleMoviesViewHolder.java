package com.example.dmitry_ko.moviesguide.ui.main.fragment.movies.view_holders;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.example.dmitry_ko.moviesguide.R;
import com.example.dmitry_ko.moviesguide.api.ApiConstants;
import com.example.dmitry_ko.moviesguide.api.model.Movie;
import com.example.dmitry_ko.moviesguide.ui.main.MainActivity;
import com.example.dmitry_ko.moviesguide.ui.main.fragment.listeners.MovieItemClickListener;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SingleMoviesViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.iv_single_poster)ImageView mPoster;
    private Movie movie;
    private MovieItemClickListener mListener;

    public SingleMoviesViewHolder(@NonNull View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        itemView.setOnClickListener(new Clicked());

        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((MainActivity)itemView.getContext()).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels;
        int widths = new Float(width * 0.7f).intValue();
        int height = ViewGroup.LayoutParams.MATCH_PARENT;

        itemView.setLayoutParams(new ViewGroup.LayoutParams(widths, height));
    }

    public void bind(Movie movie, MovieItemClickListener listener){
        this.movie = movie;
        mListener = listener;
        bindView(movie);
    }

    private void bindView(Movie movie){
        Glide.with(itemView.getContext())
                .load(ApiConstants.getPosterPath(movie.getPosterPath()))
                .into(mPoster);
    }

    private class Clicked implements View.OnClickListener{

        @Override
        public void onClick(View v) {
            mListener.movieClicked(movie);
        }
    }
}
