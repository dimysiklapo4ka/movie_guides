package com.example.dmitry_ko.moviesguide.app.di.module;

import com.example.dmitry_ko.moviesguide.app.di.scope.ActivityScope;
import com.example.dmitry_ko.moviesguide.ui.main.MainActivity;
import com.example.dmitry_ko.moviesguide.ui.main.di.MainActivityModule;
import com.example.dmitry_ko.moviesguide.ui.schedule.ScheduleActivity;
import com.example.dmitry_ko.moviesguide.ui.schedule.di.ScheduleActivityModule;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ActivityBindingsModule {

    @ContributesAndroidInjector(modules = MainActivityModule.class)
    @ActivityScope
    abstract MainActivity getMainActivity();

    @ContributesAndroidInjector(modules = ScheduleActivityModule.class)
    @ActivityScope
    abstract ScheduleActivity getWatchingActivity();
}
