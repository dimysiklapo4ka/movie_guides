package com.example.dmitry_ko.moviesguide.utils;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

public class ActivityUtils {

    public static void replaceFragment(FragmentManager fragmentManager, Fragment fragment, String tag, int containerRes){
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        if (fragmentManager.findFragmentByTag(tag) != null){
            transaction.replace(containerRes, fragmentManager.findFragmentByTag(tag));
            if (!transaction.isAddToBackStackAllowed()){
                transaction.addToBackStack(tag);
            }
            transaction.commit();
        }else {
            transaction.replace(containerRes, fragment, tag);
            if (!transaction.isAddToBackStackAllowed()){
                transaction.addToBackStack(tag);
            }
            transaction.commit();
        }
    }

}
