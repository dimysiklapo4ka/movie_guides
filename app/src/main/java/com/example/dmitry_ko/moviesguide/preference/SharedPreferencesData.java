package com.example.dmitry_ko.moviesguide.preference;

public interface SharedPreferencesData {

    Integer getLayoutManager();
    void setLayoutManager(Integer layoutManagerId);

    Integer getFilter();
    void setFilter(Integer filterId);

}
