package com.example.dmitry_ko.moviesguide.ui.main.fragment.listeners;

import com.example.dmitry_ko.moviesguide.api.model.Movie;

public interface MovieItemClickListener {
    void movieClicked(Movie movie);
}
