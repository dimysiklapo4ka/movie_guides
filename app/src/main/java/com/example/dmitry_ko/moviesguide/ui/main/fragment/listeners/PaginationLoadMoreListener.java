package com.example.dmitry_ko.moviesguide.ui.main.fragment.listeners;

public interface PaginationLoadMoreListener {
    void loadMoreMovies();
}
