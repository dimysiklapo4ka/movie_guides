package com.example.dmitry_ko.moviesguide.ui.main;

import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.example.dmitry_ko.moviesguide.R;
import com.example.dmitry_ko.moviesguide.api.model.Movie;
import com.example.dmitry_ko.moviesguide.base.BaseActivity;
import com.example.dmitry_ko.moviesguide.ui.main.fragment.listeners.LayoutMamagerListener;
import com.example.dmitry_ko.moviesguide.ui.main.fragment.listeners.MovieItemClickListener;
import com.example.dmitry_ko.moviesguide.ui.main.fragment.movie_detail.MovieDetailFragment;
import com.example.dmitry_ko.moviesguide.ui.main.fragment.movies.MoviesFragment;
import com.example.dmitry_ko.moviesguide.ui.main.listener.OnBackPresedListener;
import com.example.dmitry_ko.moviesguide.utils.ActivityUtils;

import javax.inject.Inject;

import butterknife.BindView;
import dagger.Lazy;

public class MainActivity extends BaseActivity implements MovieItemClickListener{

    private final String MOVIES_FRAGMENT = "MoviesFragment";
    private final String MOVIE_DETAIL_FRAGMENT = "MovieDetailFragment";

    @BindView(R.id.toolbar)Toolbar mToolbar;

    @Inject
    MoviesFragment moviesFragment;
    @Inject
    MovieDetailFragment movieDetailFragment;

    private LayoutMamagerListener mListener;
    private MovieItemClickListener mSendMovie;

    @Override
    protected void init() {
        replaceMovieFragm();

        setSupportActionBar(mToolbar);
        mListener = (MoviesFragment) moviesFragment;
        mSendMovie = (MovieDetailFragment) movieDetailFragment;
    }

    @Override
    public void onBackPressed() {

        if (movieDetailFragment != null){
            movieDetailFragment.onBackPresed(this);
        }else {
            super.onBackPressed();
        }
    }

    public void replaceMovieFragm(){
        ActivityUtils.replaceFragment(getSupportFragmentManager(), moviesFragment, MOVIES_FRAGMENT, R.id.mainContainer);
        mToolbar.setVisibility(View.VISIBLE);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_toolbar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_manager:
                new AlertDialog.Builder(this)
                        .setTitle("LayoutManager")
                        .setMessage("check layout manager")
                        .setPositiveButton("Linear", (dialog, which) -> mListener.setLayoutMamager(MoviesFragment.LINEAR_MANAGER))
                        .setNegativeButton("Grid", (dialog, which) -> mListener.setLayoutMamager(MoviesFragment.GRID_MANAGER))
                        .setNeutralButton("Single", ((dialog, which) -> mListener.setLayoutMamager(MoviesFragment.SINGLE_MANAGER)))
                        .create()
                        .show();
                break;
            case R.id.action_popular:
                mListener.setLayoutMamager(MoviesFragment.POPULAR_MOVIE);
                break;
            case R.id.action_highest:
                mListener.setLayoutMamager(MoviesFragment.HIGHEST_MOVIE);
                break;
            case R.id.action_newest:
                mListener.setLayoutMamager(MoviesFragment.NEWEST_MOVIE);
                break;
            case R.id.action_now_playing:
                mListener.setLayoutMamager(MoviesFragment.NOW_PLAING);
                break;
        }
        return true;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    public void movieClicked(Movie movie) {
        Log.e(MainActivity.class.getSimpleName(), "clickedItem: " + movie.getTitle());
        ActivityUtils.replaceFragment(getSupportFragmentManager(),movieDetailFragment, MOVIE_DETAIL_FRAGMENT, R.id.mainContainer);
        mSendMovie.movieClicked(movie);
        mToolbar.setVisibility(View.GONE);
    }
}
