package com.example.dmitry_ko.moviesguide.ui.main.fragment.movies.view_holders;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.dmitry_ko.moviesguide.R;
import com.example.dmitry_ko.moviesguide.api.ApiConstants;
import com.example.dmitry_ko.moviesguide.api.model.Movie;
import com.example.dmitry_ko.moviesguide.ui.main.fragment.listeners.MovieItemClickListener;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LinearMoviesViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.linearPosterMovie)ImageView mPoster;
    @BindView(R.id.linearNameMovie)TextView mNameMovie;
    @BindView(R.id.linearDescriptionMovie)TextView mDescription;
    private MovieItemClickListener mListener;
    private Movie movie;

    public LinearMoviesViewHolder(@NonNull View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        itemView.setOnClickListener(new Clicked());
    }

    public void bind(Movie movie, MovieItemClickListener listener) {
        mListener = listener;
        this.movie = movie;
        bindView(movie);
    }

    private void bindView(Movie movie){
        mNameMovie.setText(movie.getTitle());
        mDescription.setText(movie.getOverview());
        Glide.with(itemView.getContext())
                .load(ApiConstants.getPosterPath(movie.getPosterPath()))
                .into(mPoster);
    }

    private class Clicked implements View.OnClickListener{

        @Override
        public void onClick(View v) {
            mListener.movieClicked(movie);
        }
    }
}
