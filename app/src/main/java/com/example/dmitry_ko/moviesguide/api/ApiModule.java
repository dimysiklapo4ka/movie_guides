package com.example.dmitry_ko.moviesguide.api;

import android.content.SharedPreferences;

import com.example.dmitry_ko.moviesguide.preference.SharedPreferencesData;
import com.example.dmitry_ko.moviesguide.preference.SharedPreferencesDataImpl;

import javax.inject.Singleton;

import dagger.Binds;
import dagger.Module;

@Module
public interface ApiModule {
    @Binds
    @Singleton
    Repository provideRepository(RepositoryImpl repository);



}
