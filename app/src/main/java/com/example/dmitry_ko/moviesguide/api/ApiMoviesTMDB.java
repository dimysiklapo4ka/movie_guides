package com.example.dmitry_ko.moviesguide.api;

import com.example.dmitry_ko.moviesguide.api.model.MoviesWraper;
import com.example.dmitry_ko.moviesguide.api.model.ReviewsWrapper;
import com.example.dmitry_ko.moviesguide.api.model.VideoWrapper;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiMoviesTMDB {

    @GET(ApiConstants.POPULAR_MOVIE)
    Observable<MoviesWraper> popularMovies(@Query("language") String language,
                                           @Query("page") int page);

    @GET(ApiConstants.HIGHEST_RATED_MOVIES)
    Observable<MoviesWraper> highestRatedMovies(@Query("language") String language,
                                                @Query("page") int page);

    @GET(ApiConstants.NEWEST_MOVIES)
    Observable<MoviesWraper> newestMovies(@Query("language") String language,
                                          @Query("release_date.lte") String maxReleaseDate,
                                          @Query("vote_count.gte") int minVoteCount);

    @GET(ApiConstants.TRAILERS)
    Observable<VideoWrapper> trailers(@Path("movieId") String movieId);

    @GET(ApiConstants.REVIEWS)
    Observable<ReviewsWrapper> reviews(@Path("movieId") String movieId);

    @GET(ApiConstants.SEARCH_MOVIES)
    Observable<MoviesWraper> searchMovies(@Query("language") String language,
                                          @Query("query") String searchQuery);

    @GET(ApiConstants.NOW_PLAING)
    Observable<MoviesWraper> nowPlaing(@Query("language") String language,
                                       @Query("page") int page,
                                       @Query("region") String region);

}
