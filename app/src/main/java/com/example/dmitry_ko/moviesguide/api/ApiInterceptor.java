package com.example.dmitry_ko.moviesguide.api;

import com.example.dmitry_ko.moviesguide.BuildConfig;

import java.io.IOException;

import javax.inject.Inject;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class ApiInterceptor implements Interceptor {

    @Inject
    public ApiInterceptor() {
    }

    @Override
    public Response intercept(Chain chain) throws IOException {

        Request request = chain.request();

        HttpUrl originUrl = request.url();

        HttpUrl url = originUrl.newBuilder()
                .addQueryParameter("api_key", BuildConfig.TMDB_API_KEY)
                .build();

        return chain.proceed(request.newBuilder().url(url).build());
    }
}
