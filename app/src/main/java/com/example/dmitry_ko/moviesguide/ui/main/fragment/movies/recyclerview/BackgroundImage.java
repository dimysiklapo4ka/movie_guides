package com.example.dmitry_ko.moviesguide.ui.main.fragment.movies.recyclerview;

public interface BackgroundImage {
    void setImage(int position);
}
