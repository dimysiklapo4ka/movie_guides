package com.example.dmitry_ko.moviesguide.ui.main.fragment.movies;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.LinearSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SnapHelper;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.example.dmitry_ko.moviesguide.R;
import com.example.dmitry_ko.moviesguide.api.Repository;
import com.example.dmitry_ko.moviesguide.api.model.Movie;
import com.example.dmitry_ko.moviesguide.base.BaseFragment;
import com.example.dmitry_ko.moviesguide.preference.SharedPreferencesData;
import com.example.dmitry_ko.moviesguide.ui.main.MainActivity;
import com.example.dmitry_ko.moviesguide.ui.main.fragment.movies.recyclerview.BackgroundImage;
import com.example.dmitry_ko.moviesguide.ui.main.fragment.listeners.LayoutMamagerListener;
import com.example.dmitry_ko.moviesguide.ui.main.fragment.listeners.MovieItemClickListener;
import com.example.dmitry_ko.moviesguide.ui.main.fragment.listeners.PaginationLoadMoreListener;
import com.example.dmitry_ko.moviesguide.ui.main.fragment.movies.adapter.MoviesAdapter;
import com.example.dmitry_ko.moviesguide.ui.main.fragment.movies.recyclerview.RecyclerSnapHelper;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;

public class MoviesFragment extends BaseFragment implements MoviesContract.MainViewMovies
        , LayoutMamagerListener, PaginationLoadMoreListener, BackgroundImage {

    public static final int LINEAR_MANAGER = 0;
    public static final int GRID_MANAGER = 1;
    public static final int SINGLE_MANAGER = 5;

    public static final int POPULAR_MOVIE = 2;
    public static final int HIGHEST_MOVIE = 3;
    public static final int NEWEST_MOVIE = 4;
    public static final int NOW_PLAING = 6;

    @Inject
    MoviesContract.MainPresenter mPresenter;
    @Inject
    Repository mApi;
    @Inject
    SharedPreferencesData mData;

    private MoviesAdapter mAdapter;

    @BindView(R.id.moviesRecycler)
    RecyclerView mMoviesRV;
    @BindView(R.id.ll_root)
    ImageView mRootImage;

    private LinearLayoutManager linearManager;
    private GridLayoutManager gridManager;
    private LinearLayoutManager horizontalManager;

    private SnapHelper snapHelper;

    private MovieItemClickListener mListener;

    private int layoutManager;
    private int filter;

    @Inject
    public MoviesFragment() {
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mListener = (MainActivity) context;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        mPresenter.attach(this);
        mPresenter.setApi(mApi.getApi());
        initRecycler();
        mPresenter.getMovies(filter);
        srlLayout.setOnRefreshListener(() -> mPresenter.getMovies(filter));
    }

    private void initRecycler() {
        setLayoutMamager(layoutManager);
        snapHelper.attachToRecyclerView(mMoviesRV);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_movies;
    }

    @Override
    protected void init() {
        //mData.instance(getActivity());
        Log.e(MoviesFragment.class.getSimpleName(), "init: " + mData.toString() );
        layoutManager = mData.getLayoutManager();
        filter = mData.getFilter();
        Log.e(MoviesFragment.class.getSimpleName(), "LAYOUT_MANAGER" + layoutManager + " FILTER " + filter );


        linearManager = new LinearLayoutManager(getContext());
        gridManager = new GridLayoutManager(getContext(), 2, GridLayoutManager.VERTICAL, false);
        horizontalManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);

        snapHelper = new RecyclerSnapHelper(this);

        mAdapter = new MoviesAdapter(this);
        mAdapter.setItemClickListener(mListener);
    }

    @Override
    public void showProgressDialog() {
        srlLayout.setRefreshing(true);
    }

    @Override
    public void hidePrigressDialog() {
        if (srlLayout != null && srlLayout.isRefreshing()) {
            srlLayout.setRefreshing(false);
        }
    }

    @Override
    public void onPause() {
        mPresenter.onPause();
        super.onPause();
    }

    @Override
    public void onDestroyView() {
        mPresenter.dettach();
        mPresenter.onDestroy();
        super.onDestroyView();
    }

    @Override
    public void setAdapterData(List<Movie> movies) {
        Log.e(MoviesFragment.class.getSimpleName(), "" + movies.size());
        mAdapter.setMovies(movies);
    }

    @Override
    public void addAdapterData(List<Movie> movies) {
        Log.e(MoviesFragment.class.getSimpleName(), "" + movies.size());
        mAdapter.addMovies(movies);
    }

    @Override
    public void setLayoutMamager(int manager) {
        if (manager == LINEAR_MANAGER) {
            invalidateList(manager, linearManager);
        } else if (manager == GRID_MANAGER) {
            invalidateList(manager, gridManager);
        } else if (manager == SINGLE_MANAGER) {
            invalidateList(manager, horizontalManager);
            setImage(0);
        } else {
            mPresenter.getMovies(manager);
            mData.setFilter(manager);
            filter = manager;
        }
    }

    private void saveLayoutManager(int manager){
        mData.setLayoutManager(manager);
    }

    private void invalidateList(int manager, RecyclerView.LayoutManager layoutManager) {
        mAdapter.setTypeLayoutManager(manager);
        mMoviesRV.setLayoutManager(layoutManager);
        mMoviesRV.setAdapter(mAdapter);
        saveLayoutManager(manager);
    }

    @Override
    public void loadMoreMovies() {
        mPresenter.getMovies(filter);
    }

    @Override
    public void setImage(int position) {
        if (position != -1) {
            Glide.with(this)
                    .load(mAdapter.getMovies(position))
                    .into(mRootImage);
            mRootImage.setVisibility(View.VISIBLE);
        }else {
            mRootImage.setVisibility(View.INVISIBLE);
        }
    }
}
