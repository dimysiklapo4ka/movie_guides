package com.example.dmitry_ko.moviesguide.app;

import com.example.dmitry_ko.moviesguide.app.di.component.DaggerMoviesAppComponent;

import dagger.android.AndroidInjector;
import dagger.android.support.DaggerApplication;

public class MoviesApplication extends DaggerApplication {

        @Override
    protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
        return DaggerMoviesAppComponent.builder().context(this).build();
    }

}
