package com.example.dmitry_ko.moviesguide.ui.main.fragment.movie_detail;

import com.example.dmitry_ko.moviesguide.api.ApiMoviesTMDB;
import com.example.dmitry_ko.moviesguide.api.model.Movie;
import com.example.dmitry_ko.moviesguide.api.model.Video;
import com.example.dmitry_ko.moviesguide.base.BasePresenter;
import com.example.dmitry_ko.moviesguide.base.BaseView;

import java.util.List;

public interface MovieDetailContract {

    interface MovieDetailView extends BaseView{
        void setAdapterData(List<Video> movies);
    }

    interface MovieDetailPresenter extends BasePresenter<MovieDetailView>{
        void onPause();
        void onDestroy();

        void setApi(ApiMoviesTMDB apiMoviesTMDB);
        void getTrailers(Movie movie);
    }

}
