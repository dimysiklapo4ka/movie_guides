package com.example.dmitry_ko.moviesguide.base;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.dmitry_ko.moviesguide.R;

import butterknife.ButterKnife;
import dagger.android.support.DaggerFragment;

public abstract class BaseFragment extends DaggerFragment {

    protected SwipeRefreshLayout srlLayout;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(getLayoutId(), container, false);
        ButterKnife.bind(this, view);
        init();
        return refreshLayout(view);
    }

    private SwipeRefreshLayout refreshLayout(View view){
        srlLayout = new SwipeRefreshLayout(getActivity());
        ViewGroup.LayoutParams lp = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        srlLayout.setLayoutParams(lp);

        srlLayout.addView(view, lp);

        srlLayout.setColorSchemeResources(R.color.colorAccent);
        return srlLayout;
    }

    protected abstract int getLayoutId();

    protected abstract void init();
}
