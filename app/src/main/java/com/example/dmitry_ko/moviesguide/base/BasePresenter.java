package com.example.dmitry_ko.moviesguide.base;

public interface BasePresenter<V extends BaseView> {
    void attach(V view);
    void dettach();
}
