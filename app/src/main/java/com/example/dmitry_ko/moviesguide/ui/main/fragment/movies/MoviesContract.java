package com.example.dmitry_ko.moviesguide.ui.main.fragment.movies;

import com.example.dmitry_ko.moviesguide.api.ApiMoviesTMDB;
import com.example.dmitry_ko.moviesguide.api.model.Movie;
import com.example.dmitry_ko.moviesguide.base.BasePresenter;
import com.example.dmitry_ko.moviesguide.base.BaseView;

import java.util.List;

public interface MoviesContract {

    interface MainViewMovies extends BaseView{
        void setAdapterData(List<Movie> movies);
        void addAdapterData(List<Movie> movies);
    }

    interface MainPresenter extends BasePresenter<MainViewMovies> {

        void onPause();
        void onDestroy();

        void setApi(ApiMoviesTMDB apiMoviesTMDB);
        void getMovies(int sorting);
    }

}
