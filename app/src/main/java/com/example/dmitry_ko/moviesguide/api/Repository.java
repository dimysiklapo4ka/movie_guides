package com.example.dmitry_ko.moviesguide.api;

public interface Repository {
    ApiMoviesTMDB getApi();
}
