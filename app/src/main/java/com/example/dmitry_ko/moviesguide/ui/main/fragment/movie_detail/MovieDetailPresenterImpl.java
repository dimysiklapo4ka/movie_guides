package com.example.dmitry_ko.moviesguide.ui.main.fragment.movie_detail;

import android.util.Log;

import com.example.dmitry_ko.moviesguide.api.ApiMoviesTMDB;
import com.example.dmitry_ko.moviesguide.api.model.Movie;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class MovieDetailPresenterImpl implements MovieDetailContract.MovieDetailPresenter {

    private MovieDetailContract.MovieDetailView mView;
    private ApiMoviesTMDB mApi;
    private Disposable disposable;

    @Inject
    public MovieDetailPresenterImpl(){}

    @Override
    public void onPause() {
        if (!disposable.isDisposed()){
            disposable.dispose();
        }
    }

    @Override
    public void onDestroy() {
        if (!disposable.isDisposed()){
            disposable.dispose();
        }
    }

    @Override
    public void setApi(ApiMoviesTMDB apiMoviesTMDB) {
        mApi = apiMoviesTMDB;
    }

    @Override
    public void getTrailers(Movie movie) {
        disposable = mApi.trailers(movie.getId())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(videos -> mView.setAdapterData(videos.getVideos()), this::loging);
    }

    @Override
    public void attach(MovieDetailContract.MovieDetailView view) {
        mView = view;
    }

    @Override
    public void dettach() {
        mView = null;
    }

    private void loging(Throwable throwable){
        Log.e(MovieDetailPresenterImpl.class.getSimpleName(),throwable.getMessage(), throwable);
    }
}
