package com.example.dmitry_ko.moviesguide.api;

public class ApiConstants {
    public static final String BASE_POSTER_PATH = "https://image.tmdb.org/t/p/w342";
    public static final String BASR_BACKDROP_PATH = "https://image.tmdb.org/t/p/w780";
    public static final String YOUTUBE_VIDEO_URL = "https://www.youtube.com/watch?v=%1$s";
    public static final String YOUTUBE_THUMBNAIL_URL = "https://img.youtube.com/vi/%1$s/0.jpg";

    private ApiConstants() {
        // hide implicit public constructor
    }

    public static String getPosterPath(String posterPath) {
        return BASE_POSTER_PATH + posterPath;
    }

    public static String getBackdropPath(String backdropPath) {
        return BASR_BACKDROP_PATH + backdropPath;
    }

    public static final String POPULAR_MOVIE = "discover/movie?language=en&sort_by=popularity.desc";
    public static final String HIGHEST_RATED_MOVIES = "discover/movie?vote_count.gte=500&language=en&sort_by=vote_average.desc\"";
    public static final String NEWEST_MOVIES = "discover/movie?language=en&sort_by=release_date.desc";
    public static final String TRAILERS = "movie/{movieId}/videos";
    public static final String REVIEWS = "movie/{movieId}/reviews";
    public static final String SEARCH_MOVIES = "search/movie?language=en-US&page=1";
    public static final String NOW_PLAING = "movie/now_playing";
}
