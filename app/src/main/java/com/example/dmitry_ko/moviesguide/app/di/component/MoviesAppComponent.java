package com.example.dmitry_ko.moviesguide.app.di.component;

import android.content.Context;

import com.example.dmitry_ko.moviesguide.api.ApiModule;
import com.example.dmitry_ko.moviesguide.app.MoviesApplication;
import com.example.dmitry_ko.moviesguide.app.di.module.ActivityBindingsModule;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;

@Component(modules = {
        AndroidSupportInjectionModule.class,
        ApiModule.class,
        ActivityBindingsModule.class,
        })
@Singleton
public interface MoviesAppComponent extends AndroidInjector<MoviesApplication>{

    @Component.Builder
    interface Build {

        @BindsInstance
        MoviesAppComponent.Build context(Context context);
        MoviesAppComponent build();

    }

}
