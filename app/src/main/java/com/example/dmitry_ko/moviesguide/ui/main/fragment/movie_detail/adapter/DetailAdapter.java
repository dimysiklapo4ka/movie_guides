package com.example.dmitry_ko.moviesguide.ui.main.fragment.movie_detail.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.example.dmitry_ko.moviesguide.R;
import com.example.dmitry_ko.moviesguide.api.model.Video;
import com.example.dmitry_ko.moviesguide.ui.main.fragment.movie_detail.view_holder.DetailViewHolder;

import java.util.ArrayList;
import java.util.List;

public class DetailAdapter extends RecyclerView.Adapter<DetailViewHolder> {

    private List<Video> mVideos;

    public DetailAdapter(){
        mVideos = new ArrayList<>();
    }

    public void setVideos(List<Video> videos){
        if (videos != null){
            mVideos = videos;
            notifyDataSetChanged();
        }
    }

    @NonNull
    @Override
    public DetailViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new DetailViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.view_trailer, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull DetailViewHolder detailViewHolder, int i) {
        detailViewHolder.bind(mVideos.get(i));
    }

    @Override
    public int getItemCount(){
        return mVideos.size();
    }
}
