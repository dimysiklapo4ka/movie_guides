package com.example.dmitry_ko.moviesguide.ui.main.di;

import com.example.dmitry_ko.moviesguide.app.di.scope.ActivityScope;
import com.example.dmitry_ko.moviesguide.app.di.scope.FragmentScope;
import com.example.dmitry_ko.moviesguide.preference.SharedPreferencesData;
import com.example.dmitry_ko.moviesguide.preference.SharedPreferencesDataImpl;
import com.example.dmitry_ko.moviesguide.ui.main.fragment.movie_detail.MovieDetailContract;
import com.example.dmitry_ko.moviesguide.ui.main.fragment.movie_detail.MovieDetailPresenterImpl;
import com.example.dmitry_ko.moviesguide.ui.main.fragment.movies.MoviesContract;
import com.example.dmitry_ko.moviesguide.ui.main.fragment.movies.MoviesPresenterImpl;
import com.example.dmitry_ko.moviesguide.ui.main.fragment.movie_detail.MovieDetailFragment;
import com.example.dmitry_ko.moviesguide.ui.main.fragment.movies.MoviesFragment;

import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public interface MainActivityModule {

    @Binds
    @ActivityScope
    MoviesContract.MainPresenter provideMoviesPresenter(MoviesPresenterImpl presenter);

    @Binds
    @ActivityScope
    MovieDetailContract.MovieDetailPresenter provideDetailPresenter(MovieDetailPresenterImpl presenter);

    @ContributesAndroidInjector
    @FragmentScope
    MoviesFragment provideMoviesFragment();

    @ContributesAndroidInjector
    @FragmentScope
    MovieDetailFragment provideMovieDetailFragment();

    @Binds
    @ActivityScope
    SharedPreferencesData provideSharedPreferences(SharedPreferencesDataImpl sharedPreferencesData);

}
