package com.example.dmitry_ko.moviesguide.ui.main.fragment.movies;

import android.util.Log;

import com.example.dmitry_ko.moviesguide.api.ApiMoviesTMDB;
import com.example.dmitry_ko.moviesguide.api.model.Movie;
import com.example.dmitry_ko.moviesguide.ui.main.fragment.movie_detail.MovieDetailPresenterImpl;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.schedulers.Schedulers;

public class MoviesPresenterImpl implements MoviesContract.MainPresenter {

    private MoviesContract.MainViewMovies mView;
    private ApiMoviesTMDB mApi;

    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());

    private Disposable rxDisposable;

    @Inject
    public MoviesPresenterImpl(){}

    @Override
    public void attach(MoviesContract.MainViewMovies view) {
        mView = view;
    }

    @Override
    public void dettach() {
        mView = null;
    }

    @Override
    public void onPause() {
        if (!rxDisposable.isDisposed()){
            rxDisposable.dispose();
        }
        destroyLifecycle();
    }

    @Override
    public void onDestroy() {
        if (!rxDisposable.isDisposed()){
            rxDisposable.dispose();
        }
        mView = null;
    }

    private void destroyLifecycle(){
        mView.hidePrigressDialog();
        isLoading = true;
    }

    @Override
    public void setApi(ApiMoviesTMDB apiMoviesTMDB) {
        mApi = apiMoviesTMDB;
    }


    private int mPopularMovies = 1;
    private int mHigestMovies = 1;
    private int mNowPlaing = 1;
    private boolean isLoading = true;
    @Override
    public void getMovies(int sorting) {
        if (isLoading) {
            isLoading = false;
            if (sorting == 2) {
                rxDisposable = mApi.popularMovies(Locale.getDefault().getLanguage(),mPopularMovies)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .doOnSubscribe(mo -> mView.showProgressDialog())
                        .subscribe(moviesWraper -> {
                            setAdapterData(mPopularMovies, moviesWraper.getMovieList());
                            isLoading = true;
                            mView.hidePrigressDialog();
                            mPopularMovies++;
                            mHigestMovies = 1;
                            mNowPlaing = 1;
                        }, this::loging);
            } else if (sorting == 3) {
                rxDisposable = mApi.highestRatedMovies(Locale.getDefault().getLanguage(),mHigestMovies)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .doOnSubscribe(mo -> mView.showProgressDialog())
                        .subscribe(moviesWraper -> {
                            setAdapterData(mHigestMovies, moviesWraper.getMovieList());
                            isLoading = true;
                            mView.hidePrigressDialog();
                            mHigestMovies++;
                            mPopularMovies = 1;
                            mNowPlaing = 1;
                        }, this::loging);
            } else if (sorting == 4) {
                Calendar cal = Calendar.getInstance();
                rxDisposable = mApi.newestMovies(Locale.getDefault().getLanguage(),sdf.format(cal.getTime()), 50)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .doOnSubscribe(mo -> mView.showProgressDialog())
                        .subscribe(moviesWraper -> {
                            mView.setAdapterData(moviesWraper.getMovieList());
                            isLoading = true;
                            mView.hidePrigressDialog();
                            mPopularMovies = 1;
                            mHigestMovies = 1;
                            mNowPlaing = 1;
                        }, this::loging);
            } else if (sorting == 6){
                rxDisposable = mApi.nowPlaing(Locale.getDefault().getLanguage(), mNowPlaing, Locale.getDefault().getCountry())
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .doOnSubscribe(mo -> mView.showProgressDialog())
                        .subscribe(moviesWraper -> {
                            setAdapterData(mHigestMovies, moviesWraper.getMovieList());
                            isLoading = true;
                            mView.hidePrigressDialog();
                            mHigestMovies = 1;
                            mPopularMovies = 1;
                            mNowPlaing++;
                        }, this::loging);
            }
        }
    }

    private void loging(Throwable throwable){
        Log.e(MoviesPresenterImpl.class.getSimpleName(), throwable.getMessage(), throwable);
        mView.hidePrigressDialog();
    }

    private void setAdapterData(int rate, List<Movie> movies){
        if (rate > 1){
            Log.e("####", "setAdapterData: " + rate);
            mView.addAdapterData(movies);
        }else {
            Log.e("@@@@", "setAdapterData: " + rate);
            mView.setAdapterData(movies);
        }
    }
}
