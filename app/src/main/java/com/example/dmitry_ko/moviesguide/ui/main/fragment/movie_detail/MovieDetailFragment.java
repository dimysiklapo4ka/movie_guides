package com.example.dmitry_ko.moviesguide.ui.main.fragment.movie_detail;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.dmitry_ko.moviesguide.R;
import com.example.dmitry_ko.moviesguide.api.ApiConstants;
import com.example.dmitry_ko.moviesguide.api.ApiMoviesTMDB;
import com.example.dmitry_ko.moviesguide.api.Repository;
import com.example.dmitry_ko.moviesguide.api.model.Movie;
import com.example.dmitry_ko.moviesguide.api.model.Video;
import com.example.dmitry_ko.moviesguide.base.BaseFragment;
import com.example.dmitry_ko.moviesguide.ui.main.MainActivity;
import com.example.dmitry_ko.moviesguide.ui.main.fragment.movies.MoviesContract;
import com.example.dmitry_ko.moviesguide.ui.main.fragment.movies.MoviesPresenterImpl;
import com.example.dmitry_ko.moviesguide.ui.main.fragment.listeners.MovieItemClickListener;
import com.example.dmitry_ko.moviesguide.ui.main.fragment.movie_detail.adapter.DetailAdapter;
import com.example.dmitry_ko.moviesguide.ui.main.listener.OnBackPresedListener;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;

public class MovieDetailFragment extends BaseFragment implements MovieDetailContract.MovieDetailView, MovieItemClickListener, OnBackPresedListener {

    @BindView(R.id.detail_poster)
    ImageView mPoster;
    @BindView(R.id.detail_name_movie)
    TextView mName;
    @BindView(R.id.detail_description_movie)
    TextView mDescription;
    @BindView(R.id.movie_year)
    TextView mYear;
    @BindView(R.id.movie_rating)
    TextView mRating;
    @BindView(R.id.trailers)
    RecyclerView mTrailers;

    private Movie movie;
    private DetailAdapter mAdapter;

    @Inject
    MovieDetailContract.MovieDetailPresenter mPresenter;

    @Inject
    Repository mApi;

    @Inject
    public MovieDetailFragment(){}

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mPresenter.attach(this);
        mPresenter.setApi(mApi.getApi());
        initTrailers();
        setData();
        mPresenter.getTrailers(movie);
    }

    @Override
    public void onPause() {
        super.onPause();
        mPresenter.onPause();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mPresenter.onDestroy();
        mPresenter.dettach();
    }

    private void initTrailers() {
        mTrailers.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        mAdapter = new DetailAdapter();
        mTrailers.setAdapter(mAdapter);
    }

    private void setData(){
        if (movie != null) {
            Glide.with(getContext())
                    .load(ApiConstants.getBackdropPath(movie.getBackdropPath()))
                    .into(mPoster);
            mName.setText(movie.getTitle());
            mYear.setText(String.format("Release Date: %s", movie.getReleaseDate()));
            mRating.setText(String.format("Rating movies : %s/10", String.valueOf(movie.getVoteAverage())));
            mDescription.setText(movie.getOverview());
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_detail_movie;
    }

    @Override
    protected void init() {

    }

    @Override
    public void movieClicked(Movie movie) {
        this.movie = movie;
    }

    @Override
    public void onBackPresed(Activity activity) {
        MainActivity act = (MainActivity)activity;
        act.replaceMovieFragm();
    }

    @Override
    public void setAdapterData(List<Video> movies) {
        mAdapter.setVideos(movies);
    }

    @Override
    public void showProgressDialog() {

    }

    @Override
    public void hidePrigressDialog() {

    }
}
